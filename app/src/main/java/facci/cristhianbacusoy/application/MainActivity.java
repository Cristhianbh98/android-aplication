package facci.cristhianbacusoy.application;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnConvert;
    TextView result;
    EditText input;
    Double inputNumber;
    RadioButton celsius, fahrenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("MainActivity", "Cristhian Jossué Bacusoy Holgín");

        btnConvert = (Button) findViewById(R.id.btnConvert);
        result = (TextView) findViewById(R.id.result);
        input = (EditText) findViewById(R.id.input);

        celsius = (RadioButton) findViewById(R.id.celsius);
        fahrenheit = (RadioButton) findViewById(R.id.fahrenheit);

        btnConvert.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputText = input.getText().toString();

                if (inputText.isEmpty()) {
                    result.setText("Por favor ingresa una cantidad.");
                } else {
                    inputNumber = Double.parseDouble(inputText);

                    if (inputNumber <= 0) {
                        result.setText("Por favor ingresa una cantidad mayor a 0.");
                    } else {
                       if (celsius.isChecked() || fahrenheit.isChecked()) {
                           if (celsius.isChecked()) {
                               Double r = (inputNumber - 32) * 5/9 ;
                               result.setText(String.valueOf(r) + " C");
                           } else {
                               Double r = (inputNumber * 9) /5 + 32 ;
                               result.setText(String.valueOf(r) + " F");
                           }
                       } else {
                           result.setText("Por favor seleccionar un convertidor");
                       }
                    }
                }
            }
        });
    }
}